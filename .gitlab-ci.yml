---
image: debian:bullseye-slim

variables:
  pip: pip3 --timeout 100 --retries 10
  # speed up git checkout phase
  GIT_DEPTH: 1

before_script:
  - export LC_ALL=C.UTF-8
  - export DEBIAN_FRONTEND=noninteractive
  - echo Etc/UTC > /etc/timezone
  - echo 'quiet "1";'
        'APT::Install-Recommends "0";'
        'APT::Install-Suggests "0";'
        'APT::Acquire::Retries "20";'
        'APT::Get::Assume-Yes "true";'
        'Dpkg::Use-Pty "0";'
      > /etc/apt/apt.conf.d/99gitlab
  - apt-get update


black:
  script:
    - apt-get install black
    - black --check --diff --color --skip-string-normalization
        setup.py issuebot issuebot.py
        modules
        test_issuebot.py
        .gitlab


# install this locally on your setup using hooks/install-hooks.sh
hooks/pre-commit:
  script:
    - apt-get install bash dash pycodestyle pyflakes3 yamllint
    - hooks/pre-commit


bandit:
  script:
    - apt-get install python3-pip yamllint
    - yamllint .bandit
    - $pip install bandit
    - bandit --confidence-level medium --configfile .bandit --recursive $CI_PROJECT_DIR

php:
  only:
    changes:
      - .gitlab-ci.yml
      - "*/*.php"
  script:
    - apt-get install php-cli
    - php -l */*.php


pylint:
  script:
    - apt-get install pylint
    - pylint --rcfile=.pylint-rcfile --output-format=colorized --reports=n *.py */*.py


safety:
  script:
    - apt-get install ca-certificates python3-pip
    - $pip install safety
    # 40291,42559 - vulns that safety fails to see as fixed in Debian packages
    - safety check --full-report --ignore 40291 --ignore 42559


tests:
  image: registry.gitlab.com/fdroid/fdroidserver:buildserver-stretch
  script:
    - apt-get update
    - apt-get dist-upgrade
    - apt-get install -t stretch-backports
          php-cli
          python3-babel
          python3-nose
          python3-pip
          python3-setuptools
          python3-venv
          python3-wheel

    - pyvenv --system-site-packages --clear issuebot-env
    - . issuebot-env/bin/activate
    - $pip install -e .
    # make sure nose runs with the venv's python
    - python3 `which nosetests3` --nocapture --exe
